# USB Volume Wheel 
This project requires an Arduino Micro Pro (MCU) and a potential meter to create a USB volume wheel for laptops/desktops.

## Demo video
[![](http://img.youtube.com/vi/jx9MAXydEZ4/0.jpg)](http://www.youtube.com/watch?v=jx9MAXydEZ4 "Volume wheel demo video")
