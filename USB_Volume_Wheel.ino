#include "HID-Project.h"

#define ROTARY_A 9
#define ROTARY_B 8
#define ROTARY_C 7

int pinALast;
int pinCLast;

void setup() {
  Serial.begin(9600);
  Serial.write("Starting...\n");
  Serial.end();
  
  pinMode(ROTARY_A, INPUT_PULLUP);
  pinMode(ROTARY_B, INPUT_PULLUP);
  pinMode(ROTARY_C, INPUT_PULLUP);

  Consumer.begin();

  delay(200);
}

void loop() {
  handleTurns(digitalRead(ROTARY_A), digitalRead(ROTARY_B));
  handleClicks(digitalRead(ROTARY_C));
}

// Increments PC volume with poteniometer wheel
void handleTurns(int aVal, int bVal) {
  
  // Check if knob is rotating
  if (aVal != pinALast && aVal == LOW) {
    Serial.print("Rotated: ");
    
    // Determine direction (if pin A changed first, it's rotating clockwise)
    if (bVal != aVal) {
      Serial.println("clockwise");
      Consumer.write(MEDIA_VOL_UP);
    
    // Otherwise, B changed first and we're moving counterclockwise
    } else {
      Serial.println("counterclockwise");
      Consumer.write(MEDIA_VOL_DOWN);
    }
  }
  pinALast = aVal;
}

// Toggles PC volume mute with push-button state
void handleClicks(int cVal) {
  if (cVal == HIGH && !pinCLast) {
    Consumer.write(MEDIA_VOL_MUTE);
  }
  pinCLast = cVal;
}
